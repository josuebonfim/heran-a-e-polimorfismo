# Área de um Triangulo Qualquer

Seja um triangulo qualquer, onde:

- a → lado maior
- b → lado médio
- c → lado menor

Logo, a altura será: 
$$
h = \sqrt{c^2 - (\frac{c^2 - b^2 + a^2}{2a})^2}
$$

Portanto a área de um triangulo qualquer será sempre 
$$
Area = \frac{h \cdot a}{2}
$$

# Área de um Paralelogramo Qualquer

Seja um paralelogramo qualquer, onde:

- a → lado menor
- b → base

Logo, a altura será:

$$
h = b - 2x \\
x = \frac{4b - \sqrt{20a^2 - 4b^2}}{10}
$$

Portanto, a área de um paralelogramo qualquer será:

$$
Area = b \cdot h
$$

# Área de um Pentágono Qualquer

A área de um pentágono regular é dada por 

$$
Area = \frac{p \cdot a}{2}
$$

onde $a$ é a apótema. Seja o pentágono regular de lado $l$, logo:

$$ 
a = \frac{l}{2} \cdot tan(54\degree)
$$

# Área de um Hexágono Qualquer

A área de um hexágono regular é dada por 

```math
Area = \frac{p \cdot a}{2}
```

onde $a$ é a apótema. Seja o hexágono regular de lado $l$, logo:

$$ 
a = \frac{l}{2} \cdot tan(60\degree)
$$

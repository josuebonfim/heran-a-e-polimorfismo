#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"
#include <iostream>

using namespace std;

class Circulo : public FormaGeometrica{

    private:
    float raio;
    Circulo();

    public:
    Circulo(float raio);
    ~Circulo();
    float get_raio();
    void set_raio(float raio);
    float calcula_perimetro(float raio);
    void imprime_dados();
};

#endif
#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <iostream>
#include "formageometrica.hpp"

class Hexagono : public FormaGeometrica{
    private: 
        float apotema;
        float lado;
        Hexagono();
    public:
        Hexagono(float lado);
        ~Hexagono();
        float get_lado();
        void set_lado(float lado);
        float get_apotema();
        void set_apotema(float apotema);
        float calcula_apotema();
        float calcula_perimetro();
        void imprime_dados();
};


#endif
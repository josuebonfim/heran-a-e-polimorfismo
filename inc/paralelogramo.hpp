#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include <iostream>
#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica{
    private:
        Paralelogramo();
        float lado;
        float angulo;
    public:
        Paralelogramo(float lado, float base, float angulo);
        ~Paralelogramo();
        float get_lado();
        void set_lado(float lado);
        float get_angulo();
        void set_angulo(float angulo);
        float calcula_altura();
        void imprime_dados();
};

#endif
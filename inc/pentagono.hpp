#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <iostream>
#include "paralelogramo.hpp"

class Pentagono : public FormaGeometrica{
    private: 
        float apotema;
        float lado;
        Pentagono();
    public:
        Pentagono(float lado);
        ~Pentagono();
        float get_lado();
        void set_lado(float lado);
        float get_apotema();
        void set_apotema(float apotema);
        float calcula_apotema();
        float calcula_perimetro();
        void imprime_dados();
};

#endif
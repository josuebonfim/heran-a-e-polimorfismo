#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <iostream>
#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica{

    private:
        Quadrado();

    public:
        Quadrado(float base, float altura);
        ~Quadrado();
        void imprime_dados();


};

#endif
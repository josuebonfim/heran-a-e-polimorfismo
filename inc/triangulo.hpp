#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"
#include <iostream>

using namespace std;

class Triangulo : public FormaGeometrica {
private: 
    float lado1;
    float lado2;
    float lado3;
    Triangulo();
    
public:
    Triangulo(float lado1, float lado2, float lado3);
    ~Triangulo();
    float get_lado1();
    void set_lado1(float lado1);
    float get_lado2();
    void set_lado2(float lado2);
    float get_lado3();
    void set_lado3(float lado3);
    void calcula_altura();
    float calcula_perimetro(float lado1, float lado2, float lado3);
    void imprime_dados();
};

#endif
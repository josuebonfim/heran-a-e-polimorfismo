#include "circulo.hpp"

using namespace std;

Circulo::Circulo(){}

Circulo::Circulo(float raio){
    set_tipo("circulo");
    set_raio(raio);
    set_base(raio*3.1415926);
    set_altura(raio);
}

Circulo::~Circulo(){}

float Circulo::get_raio(){
    return raio;
}

void Circulo::set_raio(float raio){
    this->raio = raio;
}

float Circulo::calcula_perimetro(float raio)
{
    return (3.1415926) * raio * 2;
}

void Circulo::imprime_dados()
{
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Perimetro: " << calcula_perimetro(get_raio()) << endl;
    cout << "Area: " << calcula_area() << endl;
}
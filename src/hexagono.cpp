#include <iostream>
#include <cmath>
#include "hexagono.hpp"

#define PI 3.14159265

Hexagono::Hexagono(){}

Hexagono::Hexagono(float lado){
    set_tipo("Hexagono");
    set_lado(lado);
    set_apotema(0);
    set_base(calcula_perimetro()/2);
    set_altura(calcula_apotema());
}

Hexagono::~Hexagono(){}

float Hexagono::get_lado(){
    return lado;
}

void Hexagono::set_lado(float lado){
    this->lado = lado;
}

float Hexagono::get_apotema(){
    return apotema;
}

void Hexagono::set_apotema(float apotema){
    this->apotema = apotema;
}

float Hexagono::calcula_apotema(){
    float a;
    a = (get_lado()/2)*tan(60*PI/180);
    set_apotema(a);
    return a;
}

float Hexagono::calcula_perimetro()
{
    return 6*get_lado();
}

void Hexagono::imprime_dados(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Area: " << calcula_area() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;
}
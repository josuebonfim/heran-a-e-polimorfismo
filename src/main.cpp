#include <iostream>
#include <string>
#include <vector>
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"


using namespace std;

int main(int argc, const char *argv[])
{
    Triangulo *triangulo;
    triangulo = new Triangulo(3, 4, 5);
    triangulo->calcula_altura();
    triangulo->imprime_dados();
    delete triangulo;
    cout << "----------------------------" << endl << endl;

    Quadrado *quadrado;
    quadrado = new Quadrado(3, 3);
    quadrado->imprime_dados();
    delete quadrado;
    cout << "----------------------------" << endl << endl;


    Circulo *circulo;
    circulo = new Circulo(5.0);
    circulo->imprime_dados();
    delete circulo;
    cout << "----------------------------" << endl << endl;


    Paralelogramo *paralelogramo;
    paralelogramo = new Paralelogramo(4, 8, 30);
    paralelogramo->imprime_dados();
    delete paralelogramo;
    cout << "----------------------------" << endl << endl;


    Pentagono *pentagono;
    pentagono = new Pentagono(4);
    pentagono->imprime_dados();
    delete pentagono;
    cout << "----------------------------" << endl << endl;


    Hexagono *hexagono;
    hexagono  = new Hexagono(3);
    hexagono->imprime_dados(); 
    delete hexagono;
    cout << "----------------------------" << endl << endl;

    return 0;
}
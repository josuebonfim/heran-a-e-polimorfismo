#include "paralelogramo.hpp"
#include <cmath>

#define PI 3.14159265

Paralelogramo::Paralelogramo(){}

Paralelogramo::Paralelogramo(float lado, float base, float angulo){
    set_tipo("Paralelogramo");
    set_base(base);
    set_lado(lado);
    set_angulo(angulo);
    set_altura(calcula_altura());
}

Paralelogramo::~Paralelogramo(){}

float Paralelogramo::get_lado()
{
    return lado;
}

void Paralelogramo::set_lado(float lado){
    this->lado = lado;
}

float Paralelogramo::get_angulo()
{
    return angulo;
}

void Paralelogramo::set_angulo(float angulo){
    this->angulo = angulo;
}

float Paralelogramo::calcula_altura(){
    float h;
    h = lado * sin(angulo*PI/180);
    return h;
}

void Paralelogramo::imprime_dados()
{
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Área: " << calcula_area()/2 << endl;
    cout << "Perímetro: " << calcula_perimetro() << endl;
}
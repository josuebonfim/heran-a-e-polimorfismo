#include <iostream>
#include <cmath>
#include "pentagono.hpp"

#define PI 3.14159265

Pentagono::Pentagono(){}

Pentagono::Pentagono(float lado){
    set_tipo("Pentagono");
    set_lado(lado);
    set_apotema(0);
    set_base(calcula_perimetro()/2);
    set_altura(calcula_apotema());
}

Pentagono::~Pentagono(){}

float Pentagono::get_lado(){
    return lado;
}

void Pentagono::set_lado(float lado){
    this->lado = lado;
}

float Pentagono::get_apotema(){
    return apotema;
}

void Pentagono::set_apotema(float apotema){
    this->apotema = apotema;
}

float Pentagono::calcula_apotema(){
    float a;
    a = (get_lado()/2)*tan(54*PI/180);
    set_apotema(a);
    return a;
}

float Pentagono::calcula_perimetro()
{
    return 5*get_lado();
}

void Pentagono::imprime_dados(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Area: " << calcula_area() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;
}
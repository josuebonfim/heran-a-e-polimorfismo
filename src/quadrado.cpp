#include <iostream>
#include "quadrado.hpp"

using namespace std;

Quadrado::Quadrado(){}

Quadrado::~Quadrado(){}

Quadrado::Quadrado(float base, float altura){
    set_tipo("quadrado");
    set_base(base);
    set_altura(altura);
}

void Quadrado::imprime_dados()
{
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Perimetro: " << calcula_perimetro() << endl;
    cout << "Area: " << calcula_area() << endl;

}

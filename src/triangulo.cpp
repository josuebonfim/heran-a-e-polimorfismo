#include <iostream>
#include <cmath>
#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo(){}

Triangulo::~Triangulo(){}

Triangulo::Triangulo(float lado1, float lado2, float lado3){
    set_tipo("triangulo");
    set_base(0.0f);
    set_altura(0.0f);
    set_lado1(lado1);
    set_lado2(lado2);
    set_lado3(lado3);
}

float Triangulo::get_lado1()
{
    return lado1;
}

void Triangulo::set_lado1(float lado1)
{
    this->lado1 = lado1;
}

float Triangulo::get_lado2()
{
    return lado2;
}

void Triangulo::set_lado2(float lado2)
{
    this->lado2 = lado2;
}

float Triangulo::get_lado3()
{
    return lado3;
}

void Triangulo::set_lado3(float lado3)
{
    this->lado3 = lado3;
}

float Triangulo::calcula_perimetro(float lado1, float lado2, float lado3)
{
    return lado1 + lado2 + lado3;
}

void Triangulo::imprime_dados(){
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Area: " << calcula_area()/2 << endl;
    cout << "Perimetro: " << calcula_perimetro(get_lado1(), get_lado2(), get_lado3()) << endl;
}

void Triangulo::calcula_altura(){
    float a, b, c, h, x;
    float temp[3];

    temp[0] = get_lado1();
    //temp[0] = lado1;
    temp[1] = get_lado2();
    //temp[1] = lado2;
    temp[2] = get_lado3();
    //temp[2] = lado3;

    sort(temp, temp + 3);

    a = temp[0];
    b = temp[1];
    c = temp[2];

    x = ((a*a - b*b + c*c)/(2*c));
    h = sqrt(a*a - x*x);

    set_altura(h);
    set_base(c);
}
Tarefa dimensionada para ser feita em 6 pomodoros

Tarefas do pomodoro 1

- [ ] Dimensionamento dos pomodoros
- [ ] Implementação da Classe Triangulo
- [ ] Teste da Classe Triangulo
- [ ] Implementação da Classe Quadrado
- [ ] Teste da Classe Quadrado

Tarefas do pomodoro 2

- [ ] Implementação da Classe Paralelogramo
- [ ] Teste da Classe Paralelogramo
- [ ] Implementação da Classe Pentágono
- [ ] Teste da Classe Pentágono
- [ ] Implementação da Classe Hexágono
- [ ] Teste da Classe Hexágono

Tarefas do pomodoro 3

- [ ] Teste das classes gerais
- [ ] Implementação da Main

Pomodoros 4, 5 e 6 por dimensionar.